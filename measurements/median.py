import csv
import statistics

csv_file_path = '/Users/mac/PycharmProjects/python-test/csv/age.csv'
age_column_header = 'age'

# This function will calculate the median age
def calculate_median_age(csv_file_path, age_column_header):
    ages = []
    with open(csv_file_path, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        ages = [int(row[age_column_header]) for row in reader if row.get(age_column_header).isdigit()]

    median_age = statistics.median(ages) if ages else None
    print(f"The median age is: {median_age}") if median_age is not None else print("No valid age data to calculate the median.")

calculate_median_age(csv_file_path, age_column_header)  # call the function to find the median
#coded by va