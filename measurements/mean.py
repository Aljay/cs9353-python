# The csv module in Python allows to read and write CSV
import csv
# The statistics module provides various statistical functions
import statistics

csv_file_path = '/Users/mac/PycharmProjects/python-test/csv/age.csv'
age_column_header = 'age'

# This function will calculate the mean age
def calculate_mean_age(csv_file_path, age_column_header):
    ages = []
    with open(csv_file_path, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        ages = [int(row[age_column_header]) for row in reader if row.get(age_column_header).isdigit()]

    mean_age = statistics.mean(ages) if ages else None
    print(f"The mean age is: {round(mean_age, 2)}") if mean_age is not None else print("No valid age data to calculate the mean.")

calculate_mean_age(csv_file_path, age_column_header) # call the function to find the mean

#coded by va