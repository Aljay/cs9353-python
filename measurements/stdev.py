import csv
import statistics

csv_file_path = '/Users/mac/PycharmProjects/python-test/csv/age.csv'
age_column_header = 'age'

def calculate_std_dev_age(csv_file_path, age_column_header):
    ages = []
    with open(csv_file_path, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        ages = [int(row[age_column_header]) for row in reader if row.get(age_column_header).isdigit()]

    std_dev_age = statistics.stdev(ages) if ages else None
    print(f"The standard deviation of ages is: {round(std_dev_age, 2)}") if std_dev_age is not None else print("No valid age data to calculate the standard deviation.")

calculate_std_dev_age(csv_file_path, age_column_header)  # Call the function to find the standard deviation
