import pandas as pd


def calculate_mean_age(file_path):
    try:
        # Read the CSV file
        data = pd.read_csv(file_path)

        # Assuming there's a column named 'Age' in the CSV
        if 'Age' in data.columns:
            mean_age = data['Age'].mean()
            print(f"The mean age is: {mean_age}")
        else:
            print("No 'Age' column found in the CSV file.")
    except FileNotFoundError:
        print(f"No file found at path: {file_path}")
    except Exception as e:
        print(f"An error occurred: {e}")


# Replace 'path_to_your_file.csv' with the actual path of your 'Age.csv' file
calculate_mean_age('Python/File/Age.csv')
