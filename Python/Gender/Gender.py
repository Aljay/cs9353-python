import csv

# Initialize variables to count the number of males and females
male_count = 0
female_count = 0
total_count = 0

# Open and read the CSV file
with open('C:\\Users\\aljay\\PycharmProjects\\cs9353-python11\\Python\\Files\\Gender.csv', 'r') as csv_file:
    csv_reader = csv.DictReader(csv_file)

    # Iterate through each row in the CSV file
    for row in csv_reader:
        gender = row['Gender'].strip().lower()  # Assuming the column with gender information is named 'Gender'

        # Check the gender and update the counts accordingly
        if gender == 'male':
            male_count += 1
        elif gender == 'female':
            female_count += 1

        total_count += 1

# Calculate the percentages
male_percentage = (male_count / total_count) * 100
female_percentage = (female_count / total_count) * 100

# Display the results in the console
print(f"Total number of individuals affected by COVID-19: {total_count}")
print(f"Males: {male_count}")
print(f"Females: {female_count}")
print(f"Percentage of Males: {male_percentage:.2f}%")
print(f"Percentage of Females: {female_percentage:.2f}%")
