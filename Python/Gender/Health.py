import csv
import statistics

# Initialize a dictionary to store the counts for each health status
health_status_counts = {
    'Mild': 0,
    'Recovered': 0,
    'Asymptomatic': 0,
    'Died': 0,
    'Severe': 0
}

# Open and read the CSV file
with open('C:\\Users\\aljay\\PycharmProjects\\cs9353-python11\\Python\\Files\\Health.csv', 'r') as csv_file:
    csv_reader = csv.DictReader(csv_file)

    # Iterate through each row in the CSV file
    for row in csv_reader:
        health_status = row['health_status'].strip()  # Assuming the column with health status is named 'Health Status'

        # Update the counts for each health status
        if health_status in health_status_counts:
            health_status_counts[health_status] += 1

# Find the mode (highest frequency) health status
mode_health_status = max(health_status_counts, key=health_status_counts.get)

# Display the results in the console
print(f"Total number of Mild cases: {health_status_counts['Mild']}")
print(f"Total number of Recovered cases: {health_status_counts['Recovered']}")
print(f"Total number of Asymptomatic cases: {health_status_counts['Asymptomatic']}")
print(f"Total number of Died cases: {health_status_counts['Died']}")
print(f"Total number of Severe cases: {health_status_counts['Severe']}")
print(f"The mode (highest frequency) health status is: {mode_health_status}")
