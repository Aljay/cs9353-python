import csv
import statistics

# Initialize a dictionary to store the counts for each province
province_counts = {
    'Metropolitan Manila': 0,
    'Cebu': 0,
    'NA': 0,
    'Rizal': 0,
    'Laguna': 0
}

# Open and read the CSV file
with open('C:\\Users\\aljay\\PycharmProjects\\cs9353-python11\\Python\\Files\\Province.csv', 'r') as csv_file:
    csv_reader = csv.DictReader(csv_file)

    # Iterate through each row in the CSV file
    for row in csv_reader:
        province = row['province'].strip()  # Assuming the column with province information is named 'Province'

        # Update the counts for each province
        if province in province_counts:
            province_counts[province] += 1

# Find the mode (highest frequency) province
mode_province = max(province_counts, key=province_counts.get)

# Display the results in the console
print(f"Total number of Metropolitan Manila cases: {province_counts['Metropolitan Manila']}")
print(f"Total number of Cebu cases: {province_counts['Cebu']}")
print(f"Total number of NA cases: {province_counts['NA']}")
print(f"Total number of Rizal cases: {province_counts['Rizal']}")
print(f"Total number of Laguna cases: {province_counts['Laguna']}")
print(f"The mode (highest frequency) province with COVID-19 cases is: {mode_province}")
